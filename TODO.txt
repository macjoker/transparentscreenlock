General features
+ detect system idle
+ suspend locking for ... (15 min, 1h, 2h, 5h) + lock
+ unlock with password or shortcut or mouse wheel code
+ add tray icon
+ grab all keyboard and mouse input (like do not allow ALT-TAB, WIN-TAB, etc...)
+ show form with settings (idle time, autostart)
+ autostart (reg Run key per user)
+ store/load settings (user folder)
- localization
- transparent - alfa value - all monitors included to put shade
- put custom image and position of it
- show password prompt or not
- one way hash password in settings
- face detection to postpone lock
- allow some keys (despite the lock)
+ find free lock icon 
- wheel code
- refactor


Settings:
- use switch user button
- transparency value
+ type of unlock (multichoice): password, shortcut, mouse wheel code (with some cotrol, shift, etc...)
+ time to lock screen
- time to lock session
- lock panel position
- show graphics, image file
- custom text on password prompt


Icon
  http://www.iconarchive.com/show/sigma-general-icons-by-iconshock/lock-icon.html
  Artist: Iconshock
  Iconset: Sigma General Icons (5 icons) 
           http://www.iconarchive.com/show/sigma-general-icons-by-iconshock.html
  License: Free for non-commercial use.
  Commercial usage: Not allowed
