﻿namespace TransparentScreenLock
{
    partial class LockPanel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
                locker.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnSwitchUser = new System.Windows.Forms.Button();
            this.lockLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnSwitchUser
            // 
            this.btnSwitchUser.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSwitchUser.Location = new System.Drawing.Point(12, 48);
            this.btnSwitchUser.Name = "btnSwitchUser";
            this.btnSwitchUser.Size = new System.Drawing.Size(249, 40);
            this.btnSwitchUser.TabIndex = 0;
            this.btnSwitchUser.Text = "Switch User";
            this.btnSwitchUser.UseVisualStyleBackColor = true;
            this.btnSwitchUser.Click += new System.EventHandler(this.btnSwitchUser_Click);
            // 
            // lockLabel
            // 
            this.lockLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lockLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lockLabel.Location = new System.Drawing.Point(12, 12);
            this.lockLabel.Name = "lockLabel";
            this.lockLabel.Size = new System.Drawing.Size(249, 23);
            this.lockLabel.TabIndex = 2;
            this.lockLabel.Text = "This computer is locked";
            this.lockLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lockLabel.MouseDown += new System.Windows.Forms.MouseEventHandler(this.LockPanel_MouseDown);
            this.lockLabel.MouseMove += new System.Windows.Forms.MouseEventHandler(this.LockPanel_MouseMove);
            this.lockLabel.MouseUp += new System.Windows.Forms.MouseEventHandler(this.LockPanel_MouseUp);
            // 
            // LockPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(273, 101);
            this.Controls.Add(this.lockLabel);
            this.Controls.Add(this.btnSwitchUser);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "LockPanel";
            this.Opacity = 0.7D;
            this.ShowInTaskbar = false;
            this.Text = "LockPanel";
            this.VisibleChanged += new System.EventHandler(this.LockPanel_VisibleChanged);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.LockPanel_MouseDown);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.LockPanel_MouseMove);
            this.MouseUp += new System.Windows.Forms.MouseEventHandler(this.LockPanel_MouseUp);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnSwitchUser;
        private System.Windows.Forms.Label lockLabel;
    }
}