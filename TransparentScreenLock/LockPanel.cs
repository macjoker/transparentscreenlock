﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MouseKeyboardActivityMonitor.WinApi;
using MouseKeyboardActivityMonitor;
using TransparentScreenLock.Properties;

namespace TransparentScreenLock
{
    public partial class LockPanel : Form
    {
        private InputLocker locker;
        
        private readonly double minOpacity = 0.6;

        private readonly double maxOpacity = 0.9;

        private bool dragging;
        private Point dragStartPoint;
        private Point dragFormPoint;

        public LockPanel()
        {
            InitializeComponent();
            this.StartPosition = FormStartPosition.Manual;
            this.Location = this.GetLocation();
            this.locker = new InputLocker(this);
        }

        private void btnSwitchUser_Click(object sender, EventArgs e)
        {
            this.Cursor = Cursors.AppStarting;
            InputLocker.SwitchWindowsUser();
            this.Cursor = Cursors.Default;
        }

        private void LockPanel_VisibleChanged(object sender, EventArgs e)
        {
            if (this.Visible)
            {
                this.BringToFront();
                this.TopLevel = true;
                this.TopMost = true;
                locker.Lock();
                ChangeOpacity(minOpacity);
            }
        }

        private Point GetLockXY()
        {
            Rectangle size = System.Windows.Forms.Screen.FromControl(this).Bounds;
            int x = this.Location.X - size.Right + this.Width;
            int y = this.Location.Y - size.Bottom + this.Height;
            if (-x >= this.Location.X)
                x = this.Location.X;
            if (-y >= this.Location.Y)
                y = this.Location.Y;
            return new Point(x, y);
        }

        private Point GetLocation()
        {
            Rectangle size = System.Windows.Forms.Screen.FromControl(this).Bounds;
            int x = Settings.Default.LockX;
            int y = Settings.Default.LockY;
            if (x < 0)
            {
                x = size.Right - this.Width + x;
            }
            if (y < 0)
            {
                y = size.Bottom - this.Height + y;
            }
            return new Point(x, y);
        }
                
        private void SaveLocation()
        {
            Point p = this.GetLockXY();
            Settings.Default.LockX = p.X;
            Settings.Default.LockY = p.Y;
            Settings.Default.Save();
        }

        private delegate void ChangeOpacityDelegate(double value);
        public void ChangeOpacity(double value)
        {
            if (this.InvokeRequired)
            {
                ChangeOpacityDelegate del = new ChangeOpacityDelegate(ChangeOpacity);
                object[] parameters = { value };
                this.Invoke(del, value);
            }
            else
            {                
                this.Opacity = value;
            }
        }

        public void LockPanelEnter()
        {
            if (this.Opacity != maxOpacity)
                ChangeOpacity(maxOpacity);
        }

        public void LockPanelLeave()
        {
            if (this.Opacity != minOpacity)
                ChangeOpacity(minOpacity);
        }

        private void LockPanel_MouseDown(object sender, MouseEventArgs e)
        {
            // start moving the form
            dragging = true;
            dragStartPoint = Cursor.Position;
            dragFormPoint = this.Location;
        }

        private void LockPanel_MouseMove(object sender, MouseEventArgs e)
        {
            // move the form
            if (dragging)
            {
                Point diff = Point.Subtract(Cursor.Position, new Size(dragStartPoint));
                this.Location = Point.Add(dragFormPoint, new Size(diff));
            }
        }

        private void LockPanel_MouseUp(object sender, MouseEventArgs e)
        {
            // stop moving the form
            dragging = false;
            SaveLocation();
        }        
    }
}

