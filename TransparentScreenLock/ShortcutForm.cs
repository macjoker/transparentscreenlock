﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TransparentScreenLock
{
    public partial class ShortcutForm : Form
    {
        public string KeyString
        {
            get;
            set;
        }

        public string Modifiers
        {
            get;
            set;
        }

        public ShortcutForm()
        {
            InitializeComponent();
            KeyString = "";
            Modifiers = "";
        }

        private void keyBox_KeyDown(object sender, KeyEventArgs e)
        {            
            e.SuppressKeyPress = true;
            e.Handled = true;
            if (e.Shift || e.Alt || e.Control || e.KeyCode == Keys.LWin || e.KeyCode == Keys.RWin)
                return;
            this.KeyString = e.KeyCode.ToString();
            this.keyBox.Text = this.KeyString;            
        }

        protected override bool ProcessTabKey(bool forward)
        {
            return true;
        }

        private void ShortcutForm_VisibleChanged(object sender, EventArgs e)
        {
            const string defaulfMod = "0000000";
            if (this.Visible)
            {
                this.keyBox.Text = this.KeyString;
                if (Modifiers.Length != defaulfMod.Length)
                    this.Modifiers = defaulfMod;
                foreach (var ch in this.Modifiers)                
                    if (ch != '0' && ch != '1')
                        this.Modifiers = defaulfMod;
                
                checkShift.Checked = Modifiers[0] == '1';
                checkControl.Checked = Modifiers[1] == '1';
                checkWin.Checked = Modifiers[2] == '1';
                checkAlt.Checked = Modifiers[3] == '1';
                checkLeft.Checked = Modifiers[4] == '1';
                checkMiddle.Checked = Modifiers[5] == '1';
                checkRight.Checked = Modifiers[6] == '1';
            }
            else
            {
                this.KeyString = this.keyBox.Text;
                string mods = checkShift.Checked ? "1" : "0";
                mods += checkControl.Checked ? "1" : "0";
                mods += checkWin.Checked ? "1" : "0";
                mods += checkAlt.Checked ? "1" : "0";
                mods += checkLeft.Checked ? "1" : "0";
                mods += checkMiddle.Checked ? "1" : "0";
                mods += checkRight.Checked ? "1" : "0";
                this.Modifiers = mods;
            }
        }

        private void clearBtn_Click(object sender, EventArgs e)
        {
            this.keyBox.Text = "";            
        }

        private void ShortcutForm_Load(object sender, EventArgs e)
        {
            
        }
    }
}
