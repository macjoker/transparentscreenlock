﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace TransparentScreenLock
{
    internal class NativeMethods
    {
        internal static readonly int WTS_CURRENT_SESSION = -1;
        internal static readonly IntPtr WTS_CURRENT_SERVER_HANDLE = IntPtr.Zero;

        [DllImport("wtsapi32.dll", SetLastError = true)]
        internal static extern bool WTSDisconnectSession(IntPtr hServer, int sessionId, bool bWait);

        [DllImport("User32.dll", SetLastError = true)]
        internal static extern bool GetLastInputInfo(ref LASTINPUTINFO info);

        [DllImport("Kernel32.dll")]
        internal static extern uint GetLastError();
    }
}
