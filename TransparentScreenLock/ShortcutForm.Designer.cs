﻿namespace TransparentScreenLock
{
    partial class ShortcutForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.checkShift = new System.Windows.Forms.CheckBox();
            this.checkControl = new System.Windows.Forms.CheckBox();
            this.checkWin = new System.Windows.Forms.CheckBox();
            this.checkAlt = new System.Windows.Forms.CheckBox();
            this.checkLeft = new System.Windows.Forms.CheckBox();
            this.checkMiddle = new System.Windows.Forms.CheckBox();
            this.checkRight = new System.Windows.Forms.CheckBox();
            this.keyBox = new System.Windows.Forms.TextBox();
            this.groupKeys = new System.Windows.Forms.GroupBox();
            this.groupClick = new System.Windows.Forms.GroupBox();
            this.keyLabel = new System.Windows.Forms.Label();
            this.okBtn = new System.Windows.Forms.Button();
            this.cancelBtn = new System.Windows.Forms.Button();
            this.clearBtn = new System.Windows.Forms.Button();
            this.groupKeys.SuspendLayout();
            this.groupClick.SuspendLayout();
            this.SuspendLayout();
            // 
            // checkShift
            // 
            this.checkShift.AutoSize = true;
            this.checkShift.Location = new System.Drawing.Point(15, 23);
            this.checkShift.Name = "checkShift";
            this.checkShift.Size = new System.Drawing.Size(47, 17);
            this.checkShift.TabIndex = 0;
            this.checkShift.Text = "Shift";
            this.checkShift.UseVisualStyleBackColor = true;
            // 
            // checkControl
            // 
            this.checkControl.AutoSize = true;
            this.checkControl.Location = new System.Drawing.Point(15, 47);
            this.checkControl.Name = "checkControl";
            this.checkControl.Size = new System.Drawing.Size(59, 17);
            this.checkControl.TabIndex = 1;
            this.checkControl.Text = "Control";
            this.checkControl.UseVisualStyleBackColor = true;
            // 
            // checkWin
            // 
            this.checkWin.AutoSize = true;
            this.checkWin.Location = new System.Drawing.Point(15, 71);
            this.checkWin.Name = "checkWin";
            this.checkWin.Size = new System.Drawing.Size(45, 17);
            this.checkWin.TabIndex = 2;
            this.checkWin.Text = "Win";
            this.checkWin.UseVisualStyleBackColor = true;
            // 
            // checkAlt
            // 
            this.checkAlt.AutoSize = true;
            this.checkAlt.Location = new System.Drawing.Point(15, 95);
            this.checkAlt.Name = "checkAlt";
            this.checkAlt.Size = new System.Drawing.Size(38, 17);
            this.checkAlt.TabIndex = 3;
            this.checkAlt.Text = "Alt";
            this.checkAlt.UseVisualStyleBackColor = true;
            // 
            // checkLeft
            // 
            this.checkLeft.AutoSize = true;
            this.checkLeft.Location = new System.Drawing.Point(6, 23);
            this.checkLeft.Name = "checkLeft";
            this.checkLeft.Size = new System.Drawing.Size(44, 17);
            this.checkLeft.TabIndex = 4;
            this.checkLeft.Text = "Left";
            this.checkLeft.UseVisualStyleBackColor = true;
            // 
            // checkMiddle
            // 
            this.checkMiddle.AutoSize = true;
            this.checkMiddle.Location = new System.Drawing.Point(6, 47);
            this.checkMiddle.Name = "checkMiddle";
            this.checkMiddle.Size = new System.Drawing.Size(57, 17);
            this.checkMiddle.TabIndex = 5;
            this.checkMiddle.Text = "Middle";
            this.checkMiddle.UseVisualStyleBackColor = true;
            // 
            // checkRight
            // 
            this.checkRight.AutoSize = true;
            this.checkRight.Location = new System.Drawing.Point(6, 71);
            this.checkRight.Name = "checkRight";
            this.checkRight.Size = new System.Drawing.Size(51, 17);
            this.checkRight.TabIndex = 6;
            this.checkRight.Text = "Right";
            this.checkRight.UseVisualStyleBackColor = true;
            // 
            // keyBox
            // 
            this.keyBox.Location = new System.Drawing.Point(55, 155);
            this.keyBox.Name = "keyBox";
            this.keyBox.ShortcutsEnabled = false;
            this.keyBox.Size = new System.Drawing.Size(111, 20);
            this.keyBox.TabIndex = 7;
            this.keyBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.keyBox_KeyDown);
            // 
            // groupKeys
            // 
            this.groupKeys.Controls.Add(this.checkShift);
            this.groupKeys.Controls.Add(this.checkControl);
            this.groupKeys.Controls.Add(this.checkWin);
            this.groupKeys.Controls.Add(this.checkAlt);
            this.groupKeys.Location = new System.Drawing.Point(12, 12);
            this.groupKeys.Name = "groupKeys";
            this.groupKeys.Size = new System.Drawing.Size(110, 123);
            this.groupKeys.TabIndex = 8;
            this.groupKeys.TabStop = false;
            this.groupKeys.Text = "Special key";
            // 
            // groupClick
            // 
            this.groupClick.Controls.Add(this.checkLeft);
            this.groupClick.Controls.Add(this.checkMiddle);
            this.groupClick.Controls.Add(this.checkRight);
            this.groupClick.Location = new System.Drawing.Point(140, 12);
            this.groupClick.Name = "groupClick";
            this.groupClick.Size = new System.Drawing.Size(101, 123);
            this.groupClick.TabIndex = 9;
            this.groupClick.TabStop = false;
            this.groupClick.Text = "Mouse click";
            // 
            // keyLabel
            // 
            this.keyLabel.AutoSize = true;
            this.keyLabel.Location = new System.Drawing.Point(24, 158);
            this.keyLabel.Name = "keyLabel";
            this.keyLabel.Size = new System.Drawing.Size(25, 13);
            this.keyLabel.TabIndex = 10;
            this.keyLabel.Text = "Key";
            // 
            // okBtn
            // 
            this.okBtn.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.okBtn.Location = new System.Drawing.Point(263, 24);
            this.okBtn.Name = "okBtn";
            this.okBtn.Size = new System.Drawing.Size(75, 23);
            this.okBtn.TabIndex = 11;
            this.okBtn.Text = "OK";
            this.okBtn.UseVisualStyleBackColor = true;
            // 
            // cancelBtn
            // 
            this.cancelBtn.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelBtn.Location = new System.Drawing.Point(263, 55);
            this.cancelBtn.Name = "cancelBtn";
            this.cancelBtn.Size = new System.Drawing.Size(75, 23);
            this.cancelBtn.TabIndex = 12;
            this.cancelBtn.Text = "Cancel";
            this.cancelBtn.UseVisualStyleBackColor = true;
            // 
            // clearBtn
            // 
            this.clearBtn.Location = new System.Drawing.Point(172, 154);
            this.clearBtn.Name = "clearBtn";
            this.clearBtn.Size = new System.Drawing.Size(69, 23);
            this.clearBtn.TabIndex = 13;
            this.clearBtn.Text = "Clear";
            this.clearBtn.UseVisualStyleBackColor = true;
            this.clearBtn.Click += new System.EventHandler(this.clearBtn_Click);
            // 
            // ShortcutForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(358, 194);
            this.Controls.Add(this.clearBtn);
            this.Controls.Add(this.cancelBtn);
            this.Controls.Add(this.okBtn);
            this.Controls.Add(this.keyLabel);
            this.Controls.Add(this.groupClick);
            this.Controls.Add(this.groupKeys);
            this.Controls.Add(this.keyBox);
            this.Name = "ShortcutForm";
            this.Text = "Define shortcut";
            this.Load += new System.EventHandler(this.ShortcutForm_Load);
            this.VisibleChanged += new System.EventHandler(this.ShortcutForm_VisibleChanged);
            this.groupKeys.ResumeLayout(false);
            this.groupKeys.PerformLayout();
            this.groupClick.ResumeLayout(false);
            this.groupClick.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox checkShift;
        private System.Windows.Forms.CheckBox checkControl;
        private System.Windows.Forms.CheckBox checkWin;
        private System.Windows.Forms.CheckBox checkAlt;
        private System.Windows.Forms.CheckBox checkLeft;
        private System.Windows.Forms.CheckBox checkMiddle;
        private System.Windows.Forms.CheckBox checkRight;
        private System.Windows.Forms.TextBox keyBox;
        private System.Windows.Forms.GroupBox groupKeys;
        private System.Windows.Forms.GroupBox groupClick;
        private System.Windows.Forms.Label keyLabel;
        private System.Windows.Forms.Button okBtn;
        private System.Windows.Forms.Button cancelBtn;
        private System.Windows.Forms.Button clearBtn;
    }
}