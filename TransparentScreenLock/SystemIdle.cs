﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace TransparentScreenLock
{
    [StructLayout(LayoutKind.Sequential)]
    internal struct LASTINPUTINFO
    {
        /// <summary>
        /// The size of the structure, in bytes. This member must be set to sizeof(LASTINPUTINFO).
        /// </summary> 
        public uint size;

        /// <summary>
        ///  The tick count when the last input event was received.
        /// </summary>
        public uint ticks;
    }

    /// <summary>
    /// Get system information about idle time - time without user input.
    /// </summary>
    internal class SystemIdle
    {
        /// <summary>
        /// Get Windows information about time running without any input.
        /// </summary>
        /// <returns>Number of miliseconds system being idle.</returns>
        public static long GetIdleTickCount()
        {
            return GetTickCount() - GetLastInputTicks();
        }

        /// <summary>
        /// Get current tick count.
        /// </summary>
        /// <returns>Number of miliseconds the system is running.</returns>
        private static long GetTickCount()
        {
            return Environment.TickCount;
        }

        /// <summary>
        /// Get last input tick count.
        /// </summary>
        /// <returns>Number of miliseconds since system start till last input.</returns>
        private static long GetLastInputTicks()
        {
            LASTINPUTINFO lastInput = new LASTINPUTINFO();
            lastInput.size = (uint)Marshal.SizeOf(lastInput);
            if (!NativeMethods.GetLastInputInfo(ref lastInput))
            {
                throw new Exception(NativeMethods.GetLastError().ToString());
            }
            return lastInput.ticks;
        }
    }
}
