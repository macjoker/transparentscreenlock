﻿namespace TransparentScreenLock
{
    partial class UserSettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UserSettings));
            System.Windows.Forms.ListViewItem listViewItem4 = new System.Windows.Forms.ListViewItem("General");
            System.Windows.Forms.ListViewItem listViewItem5 = new System.Windows.Forms.ListViewItem(new string[] {
            "Locking"}, -1, System.Drawing.Color.Empty, System.Drawing.Color.Empty, null);
            System.Windows.Forms.ListViewItem listViewItem6 = new System.Windows.Forms.ListViewItem("Unlocking");
            this.btnCancel = new System.Windows.Forms.Button();
            this.trayIcon = new System.Windows.Forms.NotifyIcon(this.components);
            this.trayMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.lockToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.suspendLockingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.suspend10minMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.suspend30minMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.suspend1hMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.suspend3hMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.suspend8hMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.restoreLockingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.showConfigToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripSeparator();
            this.closeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lockTimer = new System.Windows.Forms.Timer(this.components);
            this.btnOk = new System.Windows.Forms.Button();
            this.suspendTimer = new System.Windows.Forms.Timer(this.components);
            this.settingsModuleList = new System.Windows.Forms.ListView();
            this.mainPanel = new System.Windows.Forms.Panel();
            this.unlockingPanel = new System.Windows.Forms.Panel();
            this.useWheelCode = new System.Windows.Forms.CheckBox();
            this.btnSc3 = new System.Windows.Forms.Button();
            this.btnSc2 = new System.Windows.Forms.Button();
            this.btnSc1 = new System.Windows.Forms.Button();
            this.use3sc = new System.Windows.Forms.CheckBox();
            this.use2sc = new System.Windows.Forms.CheckBox();
            this.use1sc = new System.Windows.Forms.CheckBox();
            this.useShortcuts = new System.Windows.Forms.CheckBox();
            this.useEnter = new System.Windows.Forms.CheckBox();
            this.usePassword = new System.Windows.Forms.CheckBox();
            this.passLabel = new System.Windows.Forms.Label();
            this.passwordBox = new System.Windows.Forms.TextBox();
            this.lockingPanel = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.timeoutBox = new System.Windows.Forms.TextBox();
            this.generalPanel = new System.Windows.Forms.Panel();
            this.checkAutostart = new System.Windows.Forms.CheckBox();
            this.trayMenu.SuspendLayout();
            this.mainPanel.SuspendLayout();
            this.unlockingPanel.SuspendLayout();
            this.lockingPanel.SuspendLayout();
            this.generalPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.Location = new System.Drawing.Point(430, 418);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(90, 30);
            this.btnCancel.TabIndex = 0;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // trayIcon
            // 
            this.trayIcon.ContextMenuStrip = this.trayMenu;
            this.trayIcon.Icon = ((System.Drawing.Icon)(resources.GetObject("trayIcon.Icon")));
            this.trayIcon.Text = "TransparentScreenLock";
            this.trayIcon.Visible = true;
            // 
            // trayMenu
            // 
            this.trayMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lockToolStripMenuItem,
            this.suspendLockingToolStripMenuItem,
            this.showConfigToolStripMenuItem,
            this.toolStripMenuItem2,
            this.closeToolStripMenuItem});
            this.trayMenu.Name = "contextMenuStrip1";
            this.trayMenu.Size = new System.Drawing.Size(179, 98);
            // 
            // lockToolStripMenuItem
            // 
            this.lockToolStripMenuItem.Name = "lockToolStripMenuItem";
            this.lockToolStripMenuItem.Size = new System.Drawing.Size(178, 22);
            this.lockToolStripMenuItem.Text = "Lock";
            this.lockToolStripMenuItem.Click += new System.EventHandler(this.lockTrayMenuItem_Click);
            // 
            // suspendLockingToolStripMenuItem
            // 
            this.suspendLockingToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.suspend10minMenuItem,
            this.suspend30minMenuItem,
            this.suspend1hMenuItem,
            this.suspend3hMenuItem,
            this.suspend8hMenuItem,
            this.toolStripMenuItem1,
            this.restoreLockingToolStripMenuItem});
            this.suspendLockingToolStripMenuItem.Name = "suspendLockingToolStripMenuItem";
            this.suspendLockingToolStripMenuItem.Size = new System.Drawing.Size(178, 22);
            this.suspendLockingToolStripMenuItem.Text = "Suspend locking";
            // 
            // suspend10minMenuItem
            // 
            this.suspend10minMenuItem.Name = "suspend10minMenuItem";
            this.suspend10minMenuItem.Size = new System.Drawing.Size(155, 22);
            this.suspend10minMenuItem.Text = "10 min";
            this.suspend10minMenuItem.Click += new System.EventHandler(this.suspend10minMenuItem_Click);
            // 
            // suspend30minMenuItem
            // 
            this.suspend30minMenuItem.Name = "suspend30minMenuItem";
            this.suspend30minMenuItem.Size = new System.Drawing.Size(155, 22);
            this.suspend30minMenuItem.Text = "30 min";
            this.suspend30minMenuItem.Click += new System.EventHandler(this.suspend30minMenuItem_Click);
            // 
            // suspend1hMenuItem
            // 
            this.suspend1hMenuItem.Name = "suspend1hMenuItem";
            this.suspend1hMenuItem.Size = new System.Drawing.Size(155, 22);
            this.suspend1hMenuItem.Text = "1 h";
            this.suspend1hMenuItem.Click += new System.EventHandler(this.suspend1hMenuItem_Click);
            // 
            // suspend3hMenuItem
            // 
            this.suspend3hMenuItem.Name = "suspend3hMenuItem";
            this.suspend3hMenuItem.Size = new System.Drawing.Size(155, 22);
            this.suspend3hMenuItem.Text = "3 h";
            this.suspend3hMenuItem.Click += new System.EventHandler(this.suspend3hMenuItem_Click);
            // 
            // suspend8hMenuItem
            // 
            this.suspend8hMenuItem.Name = "suspend8hMenuItem";
            this.suspend8hMenuItem.Size = new System.Drawing.Size(155, 22);
            this.suspend8hMenuItem.Text = "8 h";
            this.suspend8hMenuItem.Click += new System.EventHandler(this.suspend8hMenuItem_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(152, 6);
            // 
            // restoreLockingToolStripMenuItem
            // 
            this.restoreLockingToolStripMenuItem.Name = "restoreLockingToolStripMenuItem";
            this.restoreLockingToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
            this.restoreLockingToolStripMenuItem.Text = "Restore locking";
            this.restoreLockingToolStripMenuItem.Click += new System.EventHandler(this.restoreLockingMenuItem_Click);
            // 
            // showConfigToolStripMenuItem
            // 
            this.showConfigToolStripMenuItem.Name = "showConfigToolStripMenuItem";
            this.showConfigToolStripMenuItem.Size = new System.Drawing.Size(178, 22);
            this.showConfigToolStripMenuItem.Text = "Show configuration";
            this.showConfigToolStripMenuItem.Click += new System.EventHandler(this.showConfigurationTrayMenuItem_Click);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(175, 6);
            // 
            // closeToolStripMenuItem
            // 
            this.closeToolStripMenuItem.Name = "closeToolStripMenuItem";
            this.closeToolStripMenuItem.Size = new System.Drawing.Size(178, 22);
            this.closeToolStripMenuItem.Text = "Close";
            this.closeToolStripMenuItem.Click += new System.EventHandler(this.closeTrayMenuItem_Click);
            // 
            // lockTimer
            // 
            this.lockTimer.Enabled = true;
            this.lockTimer.Interval = 1000;
            this.lockTimer.Tick += new System.EventHandler(this.timer_Tick);
            // 
            // btnOk
            // 
            this.btnOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOk.Location = new System.Drawing.Point(342, 418);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(82, 30);
            this.btnOk.TabIndex = 3;
            this.btnOk.Text = "OK";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.okButton_Click);
            // 
            // suspendTimer
            // 
            this.suspendTimer.Tick += new System.EventHandler(this.suspendTimer_Tick);
            // 
            // settingsModuleList
            // 
            this.settingsModuleList.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            listViewItem5.StateImageIndex = 0;
            listViewItem6.StateImageIndex = 0;
            this.settingsModuleList.Items.AddRange(new System.Windows.Forms.ListViewItem[] {
            listViewItem4,
            listViewItem5,
            listViewItem6});
            this.settingsModuleList.Location = new System.Drawing.Point(12, 12);
            this.settingsModuleList.Name = "settingsModuleList";
            this.settingsModuleList.Size = new System.Drawing.Size(130, 433);
            this.settingsModuleList.TabIndex = 5;
            this.settingsModuleList.UseCompatibleStateImageBehavior = false;
            this.settingsModuleList.View = System.Windows.Forms.View.SmallIcon;
            this.settingsModuleList.SelectedIndexChanged += new System.EventHandler(this.settingsModuleList_SelectedIndexChanged);
            // 
            // mainPanel
            // 
            this.mainPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.mainPanel.Controls.Add(this.generalPanel);
            this.mainPanel.Controls.Add(this.unlockingPanel);
            this.mainPanel.Controls.Add(this.lockingPanel);
            this.mainPanel.Location = new System.Drawing.Point(148, 12);
            this.mainPanel.Name = "mainPanel";
            this.mainPanel.Size = new System.Drawing.Size(381, 385);
            this.mainPanel.TabIndex = 6;
            // 
            // unlockingPanel
            // 
            this.unlockingPanel.BackColor = System.Drawing.SystemColors.Control;
            this.unlockingPanel.Controls.Add(this.useWheelCode);
            this.unlockingPanel.Controls.Add(this.btnSc3);
            this.unlockingPanel.Controls.Add(this.btnSc2);
            this.unlockingPanel.Controls.Add(this.btnSc1);
            this.unlockingPanel.Controls.Add(this.use3sc);
            this.unlockingPanel.Controls.Add(this.use2sc);
            this.unlockingPanel.Controls.Add(this.use1sc);
            this.unlockingPanel.Controls.Add(this.useShortcuts);
            this.unlockingPanel.Controls.Add(this.useEnter);
            this.unlockingPanel.Controls.Add(this.usePassword);
            this.unlockingPanel.Controls.Add(this.passLabel);
            this.unlockingPanel.Controls.Add(this.passwordBox);
            this.unlockingPanel.ForeColor = System.Drawing.SystemColors.ControlText;
            this.unlockingPanel.Location = new System.Drawing.Point(3, 54);
            this.unlockingPanel.Name = "unlockingPanel";
            this.unlockingPanel.Size = new System.Drawing.Size(379, 367);
            this.unlockingPanel.TabIndex = 11;
            // 
            // useWheelCode
            // 
            this.useWheelCode.AutoSize = true;
            this.useWheelCode.Location = new System.Drawing.Point(14, 183);
            this.useWheelCode.Name = "useWheelCode";
            this.useWheelCode.Size = new System.Drawing.Size(103, 17);
            this.useWheelCode.TabIndex = 15;
            this.useWheelCode.Text = "Use &wheel code";
            this.useWheelCode.UseVisualStyleBackColor = true;
            this.useWheelCode.Visible = false;
            // 
            // btnSc3
            // 
            this.btnSc3.Location = new System.Drawing.Point(78, 146);
            this.btnSc3.Name = "btnSc3";
            this.btnSc3.Size = new System.Drawing.Size(75, 23);
            this.btnSc3.TabIndex = 14;
            this.btnSc3.Text = "Edit";
            this.btnSc3.UseVisualStyleBackColor = true;
            this.btnSc3.Click += new System.EventHandler(this.btnSc3_Click);
            // 
            // btnSc2
            // 
            this.btnSc2.Location = new System.Drawing.Point(78, 122);
            this.btnSc2.Name = "btnSc2";
            this.btnSc2.Size = new System.Drawing.Size(75, 23);
            this.btnSc2.TabIndex = 13;
            this.btnSc2.Text = "Edit";
            this.btnSc2.UseVisualStyleBackColor = true;
            this.btnSc2.Click += new System.EventHandler(this.btnSc2_Click);
            // 
            // btnSc1
            // 
            this.btnSc1.Location = new System.Drawing.Point(78, 98);
            this.btnSc1.Name = "btnSc1";
            this.btnSc1.Size = new System.Drawing.Size(75, 23);
            this.btnSc1.TabIndex = 12;
            this.btnSc1.Text = "Edit";
            this.btnSc1.UseVisualStyleBackColor = true;
            this.btnSc1.Click += new System.EventHandler(this.btnSc1_Click);
            // 
            // use3sc
            // 
            this.use3sc.AutoSize = true;
            this.use3sc.Location = new System.Drawing.Point(37, 150);
            this.use3sc.Name = "use3sc";
            this.use3sc.Size = new System.Drawing.Size(15, 14);
            this.use3sc.TabIndex = 11;
            this.use3sc.UseVisualStyleBackColor = true;
            // 
            // use2sc
            // 
            this.use2sc.AutoSize = true;
            this.use2sc.Location = new System.Drawing.Point(37, 127);
            this.use2sc.Name = "use2sc";
            this.use2sc.Size = new System.Drawing.Size(15, 14);
            this.use2sc.TabIndex = 10;
            this.use2sc.UseVisualStyleBackColor = true;
            // 
            // use1sc
            // 
            this.use1sc.AutoSize = true;
            this.use1sc.Location = new System.Drawing.Point(37, 104);
            this.use1sc.Name = "use1sc";
            this.use1sc.Size = new System.Drawing.Size(15, 14);
            this.use1sc.TabIndex = 9;
            this.use1sc.UseVisualStyleBackColor = true;
            // 
            // useShortcuts
            // 
            this.useShortcuts.AutoSize = true;
            this.useShortcuts.Location = new System.Drawing.Point(14, 78);
            this.useShortcuts.Name = "useShortcuts";
            this.useShortcuts.Size = new System.Drawing.Size(91, 17);
            this.useShortcuts.TabIndex = 8;
            this.useShortcuts.Text = "Use &shortcuts";
            this.useShortcuts.UseVisualStyleBackColor = true;
            // 
            // useEnter
            // 
            this.useEnter.AutoSize = true;
            this.useEnter.Location = new System.Drawing.Point(264, 38);
            this.useEnter.Name = "useEnter";
            this.useEnter.Size = new System.Drawing.Size(110, 17);
            this.useEnter.TabIndex = 7;
            this.useEnter.Text = "confirm with Enter";
            this.useEnter.UseVisualStyleBackColor = true;
            // 
            // usePassword
            // 
            this.usePassword.AutoSize = true;
            this.usePassword.Location = new System.Drawing.Point(14, 14);
            this.usePassword.Name = "usePassword";
            this.usePassword.Size = new System.Drawing.Size(93, 17);
            this.usePassword.TabIndex = 6;
            this.usePassword.Text = "Use &password";
            this.usePassword.UseVisualStyleBackColor = true;
            // 
            // passLabel
            // 
            this.passLabel.AutoSize = true;
            this.passLabel.Location = new System.Drawing.Point(34, 39);
            this.passLabel.Name = "passLabel";
            this.passLabel.Size = new System.Drawing.Size(53, 13);
            this.passLabel.TabIndex = 4;
            this.passLabel.Text = "Password";
            // 
            // passwordBox
            // 
            this.passwordBox.Location = new System.Drawing.Point(93, 36);
            this.passwordBox.Name = "passwordBox";
            this.passwordBox.PasswordChar = '*';
            this.passwordBox.Size = new System.Drawing.Size(154, 20);
            this.passwordBox.TabIndex = 3;
            // 
            // lockingPanel
            // 
            this.lockingPanel.Controls.Add(this.label1);
            this.lockingPanel.Controls.Add(this.timeoutBox);
            this.lockingPanel.Location = new System.Drawing.Point(17, 33);
            this.lockingPanel.Name = "lockingPanel";
            this.lockingPanel.Size = new System.Drawing.Size(248, 119);
            this.lockingPanel.TabIndex = 7;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(71, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Timeout [sec]";
            // 
            // timeoutBox
            // 
            this.timeoutBox.Location = new System.Drawing.Point(116, 16);
            this.timeoutBox.Name = "timeoutBox";
            this.timeoutBox.Size = new System.Drawing.Size(116, 20);
            this.timeoutBox.TabIndex = 6;
            // 
            // generalPanel
            // 
            this.generalPanel.Controls.Add(this.checkAutostart);
            this.generalPanel.Location = new System.Drawing.Point(35, 10);
            this.generalPanel.Name = "generalPanel";
            this.generalPanel.Size = new System.Drawing.Size(327, 221);
            this.generalPanel.TabIndex = 12;
            // 
            // checkAutostart
            // 
            this.checkAutostart.AutoSize = true;
            this.checkAutostart.Location = new System.Drawing.Point(21, 16);
            this.checkAutostart.Name = "checkAutostart";
            this.checkAutostart.Size = new System.Drawing.Size(146, 17);
            this.checkAutostart.TabIndex = 0;
            this.checkAutostart.Text = "Start application on logon";
            this.checkAutostart.UseVisualStyleBackColor = true;
            // 
            // UserSettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(532, 457);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.settingsModuleList);
            this.Controls.Add(this.mainPanel);
            this.Name = "UserSettings";
            this.ShowInTaskbar = false;
            this.Text = "Settings";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.UserSettings_FormClosing);
            this.Load += new System.EventHandler(this.UserSettings_Load);
            this.VisibleChanged += new System.EventHandler(this.UserSettings_VisibleChanged);
            this.trayMenu.ResumeLayout(false);
            this.mainPanel.ResumeLayout(false);
            this.unlockingPanel.ResumeLayout(false);
            this.unlockingPanel.PerformLayout();
            this.lockingPanel.ResumeLayout(false);
            this.lockingPanel.PerformLayout();
            this.generalPanel.ResumeLayout(false);
            this.generalPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.NotifyIcon trayIcon;
        private System.Windows.Forms.ContextMenuStrip trayMenu;
        private System.Windows.Forms.ToolStripMenuItem showConfigToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem closeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem lockToolStripMenuItem;
        private System.Windows.Forms.Timer lockTimer;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem suspendLockingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem suspend10minMenuItem;
        private System.Windows.Forms.ToolStripMenuItem suspend30minMenuItem;
        private System.Windows.Forms.ToolStripMenuItem suspend1hMenuItem;
        private System.Windows.Forms.ToolStripMenuItem suspend3hMenuItem;
        private System.Windows.Forms.ToolStripMenuItem suspend8hMenuItem;
        private System.Windows.Forms.Timer suspendTimer;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem restoreLockingToolStripMenuItem;
        private System.Windows.Forms.ListView settingsModuleList;
        private System.Windows.Forms.Panel mainPanel;
        private System.Windows.Forms.TextBox timeoutBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel unlockingPanel;
        private System.Windows.Forms.Label passLabel;
        private System.Windows.Forms.TextBox passwordBox;
        private System.Windows.Forms.Panel lockingPanel;
        private System.Windows.Forms.CheckBox usePassword;
        private System.Windows.Forms.CheckBox useShortcuts;
        private System.Windows.Forms.CheckBox useEnter;
        private System.Windows.Forms.CheckBox useWheelCode;
        private System.Windows.Forms.Button btnSc3;
        private System.Windows.Forms.Button btnSc2;
        private System.Windows.Forms.Button btnSc1;
        private System.Windows.Forms.CheckBox use3sc;
        private System.Windows.Forms.CheckBox use2sc;
        private System.Windows.Forms.CheckBox use1sc;
        private System.Windows.Forms.Panel generalPanel;
        private System.Windows.Forms.CheckBox checkAutostart;
    }
}

