﻿using MouseKeyboardActivityMonitor;
using MouseKeyboardActivityMonitor.WinApi;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TransparentScreenLock.Properties;

namespace TransparentScreenLock
{
    internal class InputLocker: IDisposable
    {
        private readonly KeyboardHookListener m_KeyboardHookManager;
        private readonly MouseHookListener m_MouseHookManager;

        private string passwordInput;

        private LockPanel parent;

        struct ShortcutModifiers
        {
            public bool Control;
            public bool Alt;
            public bool Shift;
            public bool Win;
            public bool Left;
            public bool Middle;
            public bool Right;
        };

        struct SpecialKeys
        {
            public bool ControlDown;
            public bool AltDown;
            public bool ShiftDown;
            public bool WinDown;
        };

        SpecialKeys specialKeys;

        private Dictionary<Keys, bool> keyPressed = new Dictionary<Keys,bool>();
        private Dictionary<MouseButtons, bool> buttonPressed = new Dictionary<MouseButtons,bool>();

        public InputLocker(LockPanel parent)
        {
            this.parent = parent;
            m_KeyboardHookManager = new KeyboardHookListener(new GlobalHooker());
            m_MouseHookManager = new MouseHookListener(new GlobalHooker());
            m_KeyboardHookManager.KeyUp += HookManager_KeyUp;
            m_KeyboardHookManager.KeyDown += HookManager_KeyDown;
            m_KeyboardHookManager.KeyPress += HookManager_KeyPress;
            m_MouseHookManager.MouseMoveExt += HookManager_Move;
            m_MouseHookManager.MouseDownExt += HookManager_DownSupress;
            foreach (var button in (MouseButtons[])Enum.GetValues(typeof(MouseButtons)))
            {
                buttonPressed[button] = false;
            }
            foreach (var key in (Keys[])Enum.GetValues(typeof(Keys)))
            {
                SetSpecialKey(key, false);
            }
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                // dispose managed resources
                m_KeyboardHookManager.Dispose();
                m_MouseHookManager.Dispose();
            }        
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void SetActiveState(bool hookEnabled)
        {
            passwordInput = "";
            m_KeyboardHookManager.Enabled = hookEnabled;
            m_MouseHookManager.Enabled = hookEnabled;
        }

        public void Lock()
        {
            SetActiveState(true);
        }

        public void Unlock()
        {
            SetActiveState(false);
            parent.Hide();
        }
                
        //##################################################################
        #region Event handlers of particular events.

        private void HookManager_KeyDown(object sender, KeyEventArgs e)
        {
            //Log(string.Format("KeyDown \t\t {0}\n", e.KeyCode));
            if (e.KeyCode != Keys.LControlKey)
                e.Handled = true;

            SetSpecialKey(e.KeyCode, true);
        }

        private void SetSpecialKey(Keys code, bool state)
        {
            keyPressed[code] = state;
            if (code == Keys.LShiftKey || code == Keys.RShiftKey)
                specialKeys.ShiftDown = state;
            if (code == Keys.LControlKey || code == Keys.RControlKey)
                specialKeys.ControlDown = state;
            if (code == Keys.LWin || code == Keys.RWin)
                specialKeys.WinDown = state;
            if (code == Keys.Alt)
                specialKeys.AltDown = state;
        }

        private void HookManager_KeyUp(object sender, KeyEventArgs e)
        {
            //Log(string.Format("KeyUp  \t\t {0}\n", e.KeyCode));
            if (e.KeyCode != Keys.LControlKey)
                e.Handled = true;
            CheckPassword(e.KeyCode); 
            SetSpecialKey(e.KeyCode, false);            
        }

        private void HookManager_KeyPress(object sender, KeyPressEventArgs e)
        {
            //Log(string.Format("KeyPress \t\t {0}\n", e.KeyChar));
            if (e.KeyChar != '\r' && e.KeyChar != '\n')
                passwordInput += e.KeyChar;                        
            e.Handled = true;
        }

        private void HookManager_Move(object sender, MouseEventExtArgs e)
        {            
            if (parent.Bounds.Contains(e.X, e.Y))
            {
                parent.LockPanelEnter();
                return;
            }
            parent.LockPanelLeave();            
        }

        private void HookManager_DownSupress(object sender, MouseEventExtArgs e)
        {
            // MouseUp event will be suppressed - release by timeout
            this.buttonPressed[e.Button] = true;
            var worker = new BackgroundWorker();
            worker.DoWork += delegate {                
                System.Threading.Thread.Sleep(300);
                this.buttonPressed[e.Button] = false;
            };
            worker.RunWorkerAsync();
            CheckPassword(Keys.None);

            if (parent.Bounds.Contains(e.X, e.Y) && e.Button != MouseButtons.Right)            
                return;
            
            e.Handled = true;
        }

        #endregion
        //##################################################################

        public void CheckPassword(Keys lastUp)
        {
            string pass = CommonUtils.Base64Decode(Settings.Default.PasswordText);
            if (Settings.Default.UsePassword && passwordInput.Length >= pass.Length)
                if (!Settings.Default.UseEnter || (Settings.Default.UseEnter && lastUp == Keys.Enter))
                    if (passwordInput.Substring(passwordInput.Length - pass.Length).Equals(pass))
                        Unlock();

            if (Settings.Default.UseShortcuts)
            {
                if (Settings.Default.UseShortcut1
                    && IsKeyValid(Settings.Default.Shortcut1Key, lastUp)
                    && IsModifierValid(Settings.Default.Shortcut1Mod))
                {
                    Unlock();
                }
                if (Settings.Default.UseShortcut2
                    && IsKeyValid(Settings.Default.Shortcut2Key, lastUp)
                    && IsModifierValid(Settings.Default.Shortcut2Mod))
                {
                    Unlock();
                }
                if (Settings.Default.UseShortcut3
                    && IsKeyValid(Settings.Default.Shortcut3Key, lastUp)
                    && IsModifierValid(Settings.Default.Shortcut3Mod))
                {
                    Unlock();
                }
            }
        }

        private bool IsKeyValid(string configKey, Keys lastUp)
        {
            //string a = lastUp.ToString();            
            //Console.Out.WriteLine(DateTime.Now + "Key: " + configKey + " - " + a);

            if (configKey.Length == 0)
                return true;
            
            if (configKey == lastUp.ToString())
                return true;
            return false;
        }

        private bool IsModifierValid(string configString)
        {
            var mods = GetModifiers(configString);
            Console.Out.WriteLine("Mod: " + configString + " - " + specialKeys.ShiftDown);
            if (mods.Win == specialKeys.WinDown
                && mods.Shift == specialKeys.ShiftDown
                && mods.Control == specialKeys.ControlDown
                && mods.Alt == specialKeys.AltDown
                && mods.Left == buttonPressed[MouseButtons.Left]
                && mods.Middle == buttonPressed[MouseButtons.Middle]
                && mods.Right == buttonPressed[MouseButtons.Right])
                return true;
            return false;
        }

        private ShortcutModifiers GetModifiers(string configString)
        {
            const string defaulfMods = "0000000";
            if (configString.Length != defaulfMods.Length)
                configString = defaulfMods;
            foreach (var ch in configString)
                if (ch != '0' && ch != '1')
                    configString = defaulfMods;
            
            ShortcutModifiers sMods;
            sMods.Shift = configString[0] == '1';
            sMods.Control = configString[1] == '1';
            sMods.Win = configString[2] == '1';
            sMods.Alt = configString[3] == '1';
            sMods.Left = configString[4] == '1';
            sMods.Middle = configString[5] == '1';
            sMods.Right = configString[6] == '1';
            return sMods;
        }

        public static void SwitchWindowsUser()
        {
            if (!NativeMethods.WTSDisconnectSession(
                NativeMethods.WTS_CURRENT_SERVER_HANDLE, NativeMethods.WTS_CURRENT_SESSION, false))
                throw new Win32Exception();        
        }
    }
}

