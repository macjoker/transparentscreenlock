﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Win32;
using TransparentScreenLock.Properties;

namespace TransparentScreenLock
{
    public partial class UserSettings : Form
    {
        readonly string autostartKey = "SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run";
        readonly string autostartName = "TransparentScreenLock";
        private bool canClose = false;
        LockPanel lockPanel = new LockPanel();
        ShortcutForm shortcutForm = new ShortcutForm();

        public UserSettings()
        {
            InitializeComponent();
            this.StartPosition = FormStartPosition.CenterScreen;
        }

        #region tray menu actions
        private void lockTrayMenuItem_Click(object sender, EventArgs e)
        {
            Lock();
        }

        private void showConfigurationTrayMenuItem_Click(object sender, EventArgs e)
        {
            this.Show();
        }

        private void closeTrayMenuItem_Click(object sender, EventArgs e)
        {
            this.canClose = true;
            this.Close();
            Application.Exit();
        }

        private void suspend10minMenuItem_Click(object sender, EventArgs e)
        {
            SuspendLocking(10 * 60 * 1000);
        }

        private void suspend30minMenuItem_Click(object sender, EventArgs e)
        {
            SuspendLocking(30 * 60 * 1000);
        }

        private void suspend1hMenuItem_Click(object sender, EventArgs e)
        {
            SuspendLocking(60 * 60 * 1000);
        }

        private void suspend3hMenuItem_Click(object sender, EventArgs e)
        {
            SuspendLocking(2 * 60 * 60 * 1000);
        }

        private void suspend8hMenuItem_Click(object sender, EventArgs e)
        {
            SuspendLocking(8 * 60 * 60 * 1000);
        }

        private void restoreLockingMenuItem_Click(object sender, EventArgs e)
        {
            lockTimer.Enabled = true;
            suspendTimer.Enabled = false;
        }

        #endregion

        private void Lock()
        {
            if (!lockPanel.Visible)
                lockPanel.ShowDialog();
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            long timeout = Settings.Default.LockTimeout * 1000;
            if (SystemIdle.GetIdleTickCount() > timeout)
            {
                Lock();
            }
        }

        private void UserSettings_VisibleChanged(object sender, EventArgs e)
        {
            Settings.Default.Reload();
            if (this.Visible)
            {
                LoadSettings();
            }            
            this.SaveLocation();
        }

        private void UserSettings_Load(object sender, EventArgs e)
        {
            this.Icon = this.trayIcon.Icon;
            this.Left = Settings.Default.LocationX;
            this.Top = Settings.Default.LocationY;
            this.generalPanel.Dock = DockStyle.Fill;
            this.lockingPanel.Dock = DockStyle.Fill;
            this.unlockingPanel.Dock = DockStyle.Fill;
            this.passwordBox.PasswordChar = '\u25CF';            
        }

        private void UserSettings_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!this.canClose)
            {
                e.Cancel = true;
                this.Hide();
            }
        }
        private void SaveLocation()
        {
            Settings.Default.LocationX = this.Left;
            Settings.Default.LocationY = this.Top;
            Settings.Default.Save();
        }

        private void LoadSettings()
        {
            RegistryKey regApp = Registry.CurrentUser.OpenSubKey(autostartKey, true);
            this.checkAutostart.Checked = regApp.GetValue(autostartName) != null;
            this.timeoutBox.Text = "" + Settings.Default.LockTimeout;

            this.passwordBox.Text = CommonUtils.Base64Decode(Settings.Default.PasswordText);
            this.usePassword.Checked = Settings.Default.UsePassword;
            this.useEnter.Checked = Settings.Default.UseEnter;

            this.useShortcuts.Checked = Settings.Default.UseShortcuts;
            this.use1sc.Checked = Settings.Default.UseShortcut1;
            this.use2sc.Checked = Settings.Default.UseShortcut2;
            this.use3sc.Checked = Settings.Default.UseShortcut3;
        }

        private void SaveSettings()
        {
            RegistryKey regApp = Registry.CurrentUser.OpenSubKey(autostartKey, true);
            if (this.checkAutostart.Checked)
                regApp.SetValue(autostartName, Application.ExecutablePath.ToString());
            else
                regApp.DeleteValue(autostartName, false);
            
            Settings.Default.LockTimeout = long.Parse(this.timeoutBox.Text);
            Settings.Default.PasswordText = CommonUtils.Base64Encode(this.passwordBox.Text);
            Settings.Default.UsePassword = this.usePassword.Checked;
            Settings.Default.UseEnter = this.useEnter.Checked;

            Settings.Default.UseShortcuts = this.useShortcuts.Checked;
            Settings.Default.UseShortcut1 = this.use1sc.Checked;
            Settings.Default.UseShortcut2 = this.use2sc.Checked;
            Settings.Default.UseShortcut3 = this.use3sc.Checked;
            
            Settings.Default.Save();
        }

        private void okButton_Click(object sender, EventArgs e)
        {
            this.SaveSettings();
            this.Hide();
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void SuspendLocking(int time)
        {
            suspendTimer.Stop();
            suspendTimer.Interval = time;
            suspendTimer.Enabled = true;
            suspendTimer.Start();
            lockTimer.Enabled = false;
        }

        private void suspendTimer_Tick(object sender, EventArgs e)
        {
            lockTimer.Enabled = true;
            suspendTimer.Enabled = false;
        }

        private void settingsModuleList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (settingsModuleList.SelectedIndices.Count <= 0)
                return;

            var index = settingsModuleList.SelectedIndices[0];
            switch (index)
            {
                case 0:
                    generalPanel.BringToFront();
                    break;
                case 1:
                    lockingPanel.BringToFront();
                    break;
                case 2:
                    unlockingPanel.BringToFront();
                    break;
            }                            
        }

        private void btnSc1_Click(object sender, EventArgs e)
        {
            shortcutForm.KeyString = Settings.Default.Shortcut1Key;
            shortcutForm.Modifiers = Settings.Default.Shortcut1Mod;
            if (shortcutForm.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                Settings.Default.Shortcut1Key = shortcutForm.KeyString;
                Settings.Default.Shortcut1Mod = shortcutForm.Modifiers;            
            }
        }

        private void btnSc2_Click(object sender, EventArgs e)
        {
            shortcutForm.KeyString = Settings.Default.Shortcut2Key;
            shortcutForm.Modifiers = Settings.Default.Shortcut2Mod;
            if (shortcutForm.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                Settings.Default.Shortcut2Key = shortcutForm.KeyString;
                Settings.Default.Shortcut2Mod = shortcutForm.Modifiers;
            }
        }

        private void btnSc3_Click(object sender, EventArgs e)
        {
            shortcutForm.KeyString = Settings.Default.Shortcut3Key;
            shortcutForm.Modifiers = Settings.Default.Shortcut3Mod;
            if (shortcutForm.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                Settings.Default.Shortcut3Key = shortcutForm.KeyString;
                Settings.Default.Shortcut3Mod = shortcutForm.Modifiers;
            }
        }

    }
}


