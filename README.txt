# TransparentScreenLock #

The application provides semi screen saver functionality. 
It can lock mouse and keyboard input but will not cover any displayed information.
Can be used instead of or together with workstation autolocking.

### What is in the repository? ###

* Provides full source code of the application
* Keeps some ideas for features to be implemented in the future

### What can be achieved with the application already? ###

* Locking mouse and keyboard input after custom idle time
* Unlocking with password or a shortcut (including mouse clicks)
* The lock screen displays only a small transparent form with information
  that a workstation is locked and an active button for user switching

### How this can be set up and launched? ###

* The solution is provided for Visual Studio 2013
* Application testing was done with Windows 8 x64
* Get these two above to build the program and start it

### Who do I talk to? ###

* MacJoker